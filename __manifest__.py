# -*- coding: utf-8 -*-
{
     'name': 'Commission Addons',
     'summary': "Module addons commission",
     'author':"Best Saler",
     'version':'12.0.1',
     'depends':['base','sale'],
     'data': [
                 'security/commission_security.xml',
                 # 'security/ir.model.access.csv',
                'views/commission_setting.xml',
              ]
}

