# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models, _
import pdb

class CommissionType(models.TransientModel):
    _inherit = 'res.config.settings'

    commission_contracts = fields.Boolean(string='Commission Contract')
    commission_products = fields.Boolean(string='Commission Product')

    # ---
    def set_values(self):
        res = super(CommissionType, self).set_values()
        self.env['ir.config_parameter'].set_param('commission_addons.commission_contracts', self.commission_contracts)
        self.env['ir.config_parameter'].set_param('commission_addons.commission_products', self.commission_products)
        return res
    # # ----
    def get_values(self):
        res = super(CommissionType, self).get_values()
        ICPSudo = self.env['ir.config_parameter'].sudo()
        commission_contract = ICPSudo.get_param('commission_addons.commission_contracts')
        commission_product = ICPSudo.get_param('commission_addons.commission_products')

        res.update(
            commission_contracts = commission_contract,
            commission_products = commission_product
        )
        return res

class CommissionContract(models.Model):
    _inherit = 'hr.contract'

    commission_percent = fields.Float('Commisson(%)')

class CommissionContract(models.Model):
    _inherit = 'product.template'

    commission_percent = fields.Float(string='Commisson(%)')

class ProductInheritSaleLine(models.Model):
    _inherit = 'sale.order.line'

    subcommission = fields.Float(string='Subcommission', compute='count_subcommission')
    commission_percent = fields.Float(related='product_id.commission_percent')

    @api.depends('price_subtotal')
    def count_subcommission(self):
        for rec in self:
            rec.subcommission = (rec.price_subtotal - (rec.purchase_price * rec.product_uom_qty)) * (rec.commission_percent/100)


class CommissionProduct(models.Model):
    _name = 'commission.product'

    product = fields.Char(string='Product')
    subcommission = fields.Float(string='Subcommission(%)')
    sale_oder_name = fields.Char(string='Sale Order')
    employee = fields.Many2one('res.users', string='Employee')

class CommissionMain(models.Model):
    _name = 'commission.main'

    margin = fields.Float(string='Margin')
    invoice_total = fields.Float(string='Invoice Total')
    commission_sale = fields.Float(string='Commission Sale')
    employee = fields.Many2one('res.users', string='Employee')
    contract_name = fields.Many2one('hr.contract', string='Contract')


class InheritSaleOrder(models.Model):
    _inherit = 'sale.order'

    contract_saler = fields.Float(related='user_id.employee_ids.contract_id.commission_percent')
    employee = fields.Char(related='user_id.employee_ids.name')
    contract_name = fields.Char(related='user_id.employee_ids.contract_id.name')
    inherit_product = fields.Many2one('sale.order.line', string='Select')
    product_name = fields.Char(related='inherit_product.product_id.name', string='Name Product')
    name_employee =fields.Char(string="Name Employee")

    @api.model
    def action_confirm(self):
        res = super(InheritSaleOrder, self).action_confirm()
        for commission_infor in self:
            vals_list_addons = {
                'margin' : commission_infor.margin,
                'invoice_total': commission_infor.amount_total,
                'commission_sale': commission_infor.margin * (commission_infor.contract_saler / 100),
                'employee': commission_infor.employee,
                'contract_name': commission_infor.contract_name
            }
            self.env['commission.main'].create(vals_list_addons)
            # # ---
            for product_line in commission_infor.order_line:
                vals_list_product = {
                    'product': product_line.name,
                    'subcommission': product_line.subcommission,
                    'sale_oder_name': product_line.name,
                    'employee': commission_infor.employee,
                }
                self.env['commission.product'].create(vals_list_product)

        return res





